﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;
using HelloTestWorld.Web.Models.Pages;
using HelloTestWorld.Web.Models.ViewModels;

namespace HelloTestWorld.Web.Controllers
{
    public class StandardPageController : PageController<StandardPage>
    {
        public ActionResult Index(StandardPage currentPage)
        {
            StandardPageViewModel viewModel = new StandardPageViewModel
            {
                CurrentPage = currentPage
            };

            return View(viewModel);
        }
    }
}