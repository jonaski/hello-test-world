﻿using System.Web.Mvc;
using EPiServer;
using EPiServer.Web.Mvc;
using HelloTestWorld.Web.Business;
using HelloTestWorld.Web.Models.Pages;
using HelloTestWorld.Web.Models.RequestModels;
using HelloTestWorld.Web.Models.ViewModels;

namespace HelloTestWorld.Web.Controllers
{
    public class StartPageController : PageController<StartPage>
    {
        private readonly IEmailService _emailService;
        private readonly IContentLoader _contentLoader;

        public StartPageController(IEmailService emailService, IContentLoader contentLoader)
        {
            _emailService = emailService;
            _contentLoader = contentLoader;
        }

        [HttpGet]
        public ActionResult Index(StartPage currentPage)
        {
            StartPageViewModel viewModel = new StartPageViewModel
            {
                CurrentPage = currentPage,
                ChildPages = _contentLoader.GetChildren<StandardPage>(currentPage.ContentLink),
                StupidProp = "I'm with stupid"
            };

            return View("sträng");
        }

        [HttpPost]
        public ActionResult Index(StartPage currentPage, EmailRequestModel emailRequestModel)
        {
            bool didSendEmail = _emailService.SendEmail(emailRequestModel.Recipient, emailRequestModel.Message);

            return RedirectToAction("Index");
        }
    }
}