﻿namespace HelloTestWorld.Web.Business
{
    public interface IEmailService
    {
        bool SendEmail(string recipient, string message);
    }
}