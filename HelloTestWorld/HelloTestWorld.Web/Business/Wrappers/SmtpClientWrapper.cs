﻿using EPiServer.ServiceLocation;
using System;
using System.Net.Mail;

namespace HelloTestWorld.Web.Business.Wrappers
{
    [ServiceConfiguration(ServiceType = typeof(ISmtpClient), Lifecycle = ServiceInstanceScope.Transient)]
    public class SmtpClientWrapper : ISmtpClient, IDisposable
    {
        private readonly SmtpClient _smtpClient;

        public SmtpClientWrapper()
        {
            _smtpClient = new SmtpClient();
        }

        public virtual bool Send(string recipient, string message)
        {
            try
            {
                _smtpClient.Send("test@test.local", recipient, "subject", message);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public void Dispose()
        {
            _smtpClient.Dispose();
        }
    }
}