﻿namespace HelloTestWorld.Web.Business.Wrappers
{
    public interface ISmtpClient
    {
        bool Send(string recipient, string message);
    }
}