﻿using HelloTestWorld.Web.Business.Wrappers;
using System;
using System.Net.Mail;

namespace HelloTestWorld.Web.Business
{
    public class EmailService : IEmailService
    {
        private readonly ISmtpClient _smtpClient;

        public EmailService(ISmtpClient smtpClient)
        {
            _smtpClient = smtpClient;
        }

        public virtual bool SendEmail(string recipient, string message)
        {
            if(string.IsNullOrEmpty(recipient) || string.IsNullOrEmpty(message))
            {
                return false;
            }

            try
            {
                
                _smtpClient.Send("test@test.local", "hej");
               
            }
            catch (Exception)
            {
                return false;
            }            

            return true;
        }
    }
}