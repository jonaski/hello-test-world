﻿using System.Collections.Generic;
using HelloTestWorld.Web.Models.Pages;

namespace HelloTestWorld.Web.Models.ViewModels
{
    public class StartPageViewModel
    {
        public StartPage CurrentPage { get; set; }
        public string StupidProp { get; set; }
        public IEnumerable<StandardPage> ChildPages { get; set; }
    }
}