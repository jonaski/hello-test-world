﻿using HelloTestWorld.Web.Models.Pages;

namespace HelloTestWorld.Web.Models.ViewModels
{
    public class StandardPageViewModel
    {
        public StandardPage CurrentPage { get; set; }
    }
}