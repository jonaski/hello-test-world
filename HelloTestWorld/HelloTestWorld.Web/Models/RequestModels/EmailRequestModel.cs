﻿namespace HelloTestWorld.Web.Models.RequestModels
{
    public class EmailRequestModel
    {
        public string Recipient { get; set; }
        public string Message { get; set; }
    }
}