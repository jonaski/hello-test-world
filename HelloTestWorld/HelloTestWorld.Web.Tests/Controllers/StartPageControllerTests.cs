﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using HelloTestWorld.Web.Business;
using HelloTestWorld.Web.Controllers;
using HelloTestWorld.Web.Models.Pages;
using HelloTestWorld.Web.Models.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HelloTestWorld.Web.Tests.Controllers
{
    [TestClass]
    public class StartPageControllerTests
    {
        private readonly Mock<IEmailService> _emailServiceMock;
        private readonly Mock<IContentLoader> _contentLoaderMock;

        private readonly StartPageController _subject;

        private List<StandardPage> _standardPages = new List<StandardPage>
        {
            new StandardPage(),
            new StandardPage(),
            new StandardPage()
        };
        
        public StartPageControllerTests()
        {
            _emailServiceMock = new Mock<IEmailService>();
            _contentLoaderMock = new Mock<IContentLoader>();

            _subject = new StartPageController(_emailServiceMock.Object, _contentLoaderMock.Object);
        }

        [TestMethod]
        public void Index_ModelShouldNotBeNull()
        {
            StartPage startPage = new StartPage();

            var result = _subject.Index(startPage) as ViewResult;
            Assert.IsNotNull(result.Model);
        }

        [TestMethod]
        public void Index_ModelShouldBeOfTypeStartPageViewModel()
        {
            StartPage startPage = new StartPage();

            var result = _subject.Index(startPage) as ViewResult;
            Assert.IsInstanceOfType(result.Model, typeof(StartPageViewModel));
        }

        [TestMethod]
        public void Index_ShouldHaveChildPages()
        {
            var result = _subject.Index(new StartPage()) as ViewResult;
            Assert.IsNotNull(((StartPageViewModel)result.Model).ChildPages);
            Assert.IsTrue(((StartPageViewModel)result.Model).ChildPages.Any());
        }
    }
}
