﻿using HelloTestWorld.Web.Business;
using HelloTestWorld.Web.Business.Wrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Net.Mail;

namespace HelloTestWorld.Web.Tests.Business
{
    [TestClass]
    public class EmailServiceTests
    {
        private readonly EmailService _subject;

        public EmailServiceTests()
        {
            Mock<ISmtpClient> smtpClientMock = new Mock<ISmtpClient>();
            smtpClientMock.Setup(x => x.Send(It.IsAny<string>(), It.IsAny<string>())).Throws<Exception>();

            _subject = new EmailService(smtpClientMock.Object);
        }

        [TestMethod]
        public void SendEmail_NullOrEmptyRecipientShouldReturnFalse()
        {
            Assert.IsFalse(_subject.SendEmail(null, "message"));
            Assert.IsFalse(_subject.SendEmail(string.Empty, "message"));
        }

        [TestMethod]
        public void SendEmail_NullOrEmptyMessageShouldReturnFalse()
        {
            Assert.IsFalse(_subject.SendEmail("recipient", null));
            Assert.IsFalse(_subject.SendEmail("recipient", string.Empty));
        }

        [TestMethod]
        public void SendEmail_FailToSendMailShouldReturnFalse()
        {
            Assert.IsFalse(_subject.SendEmail("hej@local.se", "hej"));
        }

        [TestMethod]
        public void SendEmail_ShouldReturnTrue()
        {
        }
    }
}
